package ru.trifonov.tm.view;

public class CommandForProjectManager {

    public static final String HELP = "help";
    public static final String H = "h";


//    ProjectRepository command

    public static final String PROJECT_CREATE = "project-create";
    public static final String PC = "pc";
    public static final String PROJECT_UPDATE = "project-update";
    public static final String PU = "pu";
    public static final String PROJECT_LIST = "project-list";
    public static final String PL = "pl";
    public static final String PROJECT_REMOVE = "project-remove";
    public static final String PR = "pr";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PCL = "pcl";

//    TaskRepository command

    public static final String TASK_CREATE = "task-create";
    public static final String TC = "tc";
    public static final String TASK_UPDATE = "task-update";
    public static final String TU = "tu";
    public static final String TASK_LIST = "task-list";
    public static final String TL = "tl";
    public static final String TASK_REMOVE = "task-remove";
    public static final String TR = "tr";
    public static final String TASK_CLEAR = "taskClear";
    public static final String TCL = "tcl";
}
