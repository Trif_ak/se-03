package ru.trifonov.tm.view;

import ru.trifonov.tm.api.ProjectRepository;
import ru.trifonov.tm.api.TaskRepository;
import ru.trifonov.tm.repository.IProjectRepository;
import ru.trifonov.tm.repository.ITaskRepository;

import java.util.Scanner;

public class MenuOfProjectManager {
    private Scanner scanner;
    private ProjectRepository projectService;
    private TaskRepository taskService;

    private String name;
    private String idProject;
    private String idTask;
    private String description;
    private String startDate;
    private String finishDate;


    /*Constructor*/

    public MenuOfProjectManager(Scanner scanner, ProjectRepository projectService, TaskRepository taskService) {
        this.scanner = scanner;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    /*Implementation command*/

    public void commandHelp() {
        System.out.println(" help: Show all commands. " +
                "\n project-create (pc): Create new project." +
                "\n project-update (pu): Update selected project." +
                "\n project-list (pl): Show all projects." +
                "\n project-remove (pr): Remove selected project." +
                "\n project-clear (pcl): Remove all projects." +
                "\n" +
                "\n task-create (tc): Create new task." +
                "\n task-update (tu): Update selected task." +
                "\n task-list (tl): Show all tasks." +
                "\n task-remove (tr): Remove selected task." +
                "\n task-clear (tcl): Remove all tasks." +
                "\n " +
                "\n exit: Exit from app.");
    }

    /*Command for project*/

    public void commandProjectCreate() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("Enter name");
        name = scanner.nextLine();
        System.out.println("Enter description");
        description = scanner.nextLine();
        System.out.println("Enter start date. Date format DD.MM.YYYY");
        startDate = scanner.nextLine();
        System.out.println("Enter finish date. Date format DD.MM.YYYY");
        finishDate = scanner.nextLine();
        projectService.create(name, projectService.generateRandomBasedUUID(), description, startDate, finishDate);
        System.out.println("[OK]");
    }

    public void commandProjectUpdate() {
        System.out.println("PROJECT UPDATE");
        System.out.println("Enter the ID of the project you want to update");
        idProject = scanner.nextLine();
        System.out.println("Enter new name");
        name = scanner.nextLine();
        System.out.println("Enter new description");
        description = scanner.nextLine();
        System.out.println("Enter new start date. Date format DD.MM.YYYY");
        startDate = scanner.nextLine();
        System.out.println("Enter new finish date. Date format DD.MM.YYYY");
        finishDate = scanner.nextLine();
        projectService.update(name, idProject, description, startDate, finishDate);
        System.out.println("[OK]");
    }

    public void commandProjectList() {
        System.out.println("[ALL PROJECT LIST]");
        projectService.read();
        System.out.println("[OK]");
    }

    public void commandProjectRemove() {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("Enter the ID of the project you want to delete");
        idProject = scanner.nextLine();
        taskService.clear(idProject);
        projectService.delete(idProject);
        System.out.println("[OK]");
    }

    public void commandProjectClear() {
        System.out.println("[ALL PROJECT CLEAR]");
        taskService.clearAll();
        projectService.clear();
        System.out.println("[OK]");
    }

    /*Command for task*/

    public void commandTaskCreate() {
        System.out.println("[TASK CREATE]");
        System.out.println("Enter ID of project");
        idProject = scanner.nextLine();
        System.out.println("Enter name task");
        name = scanner.nextLine();
        System.out.println("Enter description");
        description = scanner.nextLine();
        System.out.println("Enter start date task. Date format DD.MM.YYYY");
        startDate = scanner.nextLine();
        System.out.println("Enter finish date task. Date format DD.MM.YYYY");
        finishDate = scanner.nextLine();
        taskService.create(name, taskService.generateRandomBasedUUID(), idProject, description, startDate, finishDate);
        System.out.println("[OK]");
    }

    public void commandTaskUpdate() {
        System.out.println("[TASK UPDATE]");
        System.out.println("Enter the ID of the task you want to update");
        idTask = scanner.nextLine();
        System.out.println("Enter new name");
        name = scanner.nextLine();
        System.out.println("Enter new idProject");
        idProject = scanner.nextLine();
        System.out.println("Enter new description");
        description = scanner.nextLine();
        System.out.println("Enter start date task. Date format DD.MM.YYYY");
        startDate = scanner.nextLine();
        System.out.println("Enter finish date task. Date format DD.MM.YYYY");
        name = scanner.nextLine();
        taskService.update(name, idTask, idProject, description, startDate, finishDate);
        System.out.println("[OK]");
    }

    public void commandTaskList() {
        System.out.println("[TASK LIST]");
        System.out.println("Enter ID of project");
        idProject = scanner.nextLine();
        taskService.read(idProject);
        System.out.println("[OK]");
    }

    public void commandTaskRemove() {
        System.out.println("[TASK REMOVE]");
        System.out.println("Enter the ID of the task you want to delete");
        idTask = scanner.nextLine();
        taskService.delete(idTask);
        System.out.println("[OK]");
    }

    public void commandTaskClear() {
        System.out.println("[TASK CLEAR]");
        System.out.println("Enter ID of project");
        idProject = scanner.nextLine();
        taskService.clear(idProject);
        System.out.println("[OK]");
    }
}
