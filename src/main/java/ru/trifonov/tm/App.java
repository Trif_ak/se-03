package ru.trifonov.tm;

import ru.trifonov.tm.api.ProjectRepository;
import ru.trifonov.tm.api.TaskRepository;
import ru.trifonov.tm.repository.IProjectRepository;
import ru.trifonov.tm.repository.ITaskRepository;
import ru.trifonov.tm.view.CommandForProjectManager;
import ru.trifonov.tm.view.MenuOfProjectManager;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        String command;

        ProjectRepository projectService = new IProjectRepository();
        TaskRepository taskService = new ITaskRepository();
        Scanner inCommand = new Scanner(System.in);

        MenuOfProjectManager menuOfProjectManager = new MenuOfProjectManager(inCommand, projectService, taskService);


        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println("Enter \"help\" for show all commands. ");

        do {
            command = inCommand.nextLine();
            switch (command) {
                case (CommandForProjectManager.H):
                case (CommandForProjectManager.HELP):
                    menuOfProjectManager.commandHelp();
                    break;

                    /*Projects commands*/

                case (CommandForProjectManager.PC):
                case (CommandForProjectManager.PROJECT_CREATE):
                    menuOfProjectManager.commandProjectCreate();
                    break;

                case (CommandForProjectManager.PU):
                case (CommandForProjectManager.PROJECT_UPDATE):
                    menuOfProjectManager.commandProjectUpdate();
                    break;

                case (CommandForProjectManager.PL):
                case (CommandForProjectManager.PROJECT_LIST):
                    menuOfProjectManager.commandProjectList();
                    break;

                case (CommandForProjectManager.PR):
                case (CommandForProjectManager.PROJECT_REMOVE):
                    menuOfProjectManager.commandProjectRemove();
                    break;

                case (CommandForProjectManager.PCL):
                case (CommandForProjectManager.PROJECT_CLEAR):
                    menuOfProjectManager.commandProjectClear();
                    break;

                    /*TaskRepository commands*/

                case (CommandForProjectManager.TC):
                case (CommandForProjectManager.TASK_CREATE):
                    menuOfProjectManager.commandTaskCreate();
                    break;

                case (CommandForProjectManager.TU):
                case (CommandForProjectManager.TASK_UPDATE):
                    menuOfProjectManager.commandTaskUpdate();
                    break;

                case (CommandForProjectManager.TL):
                case (CommandForProjectManager.TASK_LIST):
                    menuOfProjectManager.commandTaskList();
                    break;

                case (CommandForProjectManager.TR):
                case (CommandForProjectManager.TASK_REMOVE):
                    menuOfProjectManager.commandTaskRemove();
                    break;
                case (CommandForProjectManager.TCL):
                case (CommandForProjectManager.TASK_CLEAR):
                    menuOfProjectManager.commandTaskClear();
                    break;
            }
        } while (!command.equals("exit"));
    }

}