package ru.trifonov.tm.repository;

import ru.trifonov.tm.api.ProjectRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;

public class IProjectRepository implements ProjectRepository {
    private ArrayList<ru.trifonov.tm.entity.Project> projects = new ArrayList<>();

    /*CRUD methods*/

    @Override
    public void create(String name, String id, String description, String startDate, String finishDate) {
        ru.trifonov.tm.entity.Project project = new ru.trifonov.tm.entity.Project();
        project.setName(name);
        project.setId(id);
        project.setDescription(description);
        project.setStartDate(startDate);
        project.setFinishDate(finishDate);
        projects.add(project);
    }

    @Override
    public void update(String name, String id, String description, String startDate, String finishDate) {
        for (int i = 0; i < projects.size(); i++) {
            if (projects.get(i).getId().equals(id)) {
                projects.get(i).setName(name);
                projects.get(i).setDescription(description);
                projects.get(i).setStartDate(startDate);
                projects.get(i).setFinishDate(finishDate);
            }
        }
    }

    @Override
    public void read() {
        for (ru.trifonov.tm.entity.Project project : projects) {
            System.out.println(
                    " ID  " + project.getId() +
                    "\n NAME  " + project.getName() +
                    "\n DESCRIPTION  " + project.getDescription() +
                    "\n PROJECT START DATE  " + project.getStartDate() +
                    "\n PROJECT FINISH DATE  " + project.getFinishDate());
        }
    }

    @Override
    public void delete(String id) {
        Iterator<ru.trifonov.tm.entity.Project> projectIterator = projects.iterator();
        while (projectIterator.hasNext()) {
            ru.trifonov.tm.entity.Project nextProject = projectIterator.next();
            if (nextProject.getId().equals(id)) {
                projectIterator.remove();
                break;
            }
        }
    }

    @Override
    public void clear() {
        projects.removeAll(projects);
    }

    @Override
    public String generateRandomBasedUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
