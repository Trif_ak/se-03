package ru.trifonov.tm.repository;

import ru.trifonov.tm.api.TaskRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;

public class ITaskRepository implements TaskRepository {
    private ArrayList<ru.trifonov.tm.entity.Task> tasks = new ArrayList<>();

    @Override
    public void create(String name, String id, String idProject, String description, String startDate, String finishDate) {
        ru.trifonov.tm.entity.Task task = new ru.trifonov.tm.entity.Task();
        task.setName(name);
        task.setIdTask(id);
        task.setIdProject(idProject);
        task.setDescription(description);
        task.setStartDate(startDate);
        task.setFinishDate(finishDate);
        tasks.add(task);
    }

    @Override
    public void update(String name, String id, String idProject, String description, String startDate, String finishDate) {
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).getIdTask().equals(id)) {
                tasks.get(i).setName(name);
                tasks.get(i).setDescription(description);
                tasks.get(i).setStartDate(startDate);
                tasks.get(i).setFinishDate(finishDate);
            }
        }
    }

    @Override
    public void read(String idProject) {
        for (ru.trifonov.tm.entity.Task task : tasks) {
            if (task.getIdProject().equals(idProject)) {
                System.out.println(
                        " ID  " + task.getIdTask() +
                        "\n NAME  " + task.getName() +
                        "\n DESCRIPTION  " + task.getDescription() +
                        "\n PROJECT START DATE  " + task.getStartDate() +
                        "\n PROJECT FINISH DATE  " + task.getFinishDate());
            }
        }
    }

    @Override
    public void delete(String id) {
        Iterator<ru.trifonov.tm.entity.Task> taskIterator = tasks.iterator();
        while (taskIterator.hasNext()) {
            ru.trifonov.tm.entity.Task nextTask = taskIterator.next();
            if (nextTask.getIdTask().equals(id)) {
                taskIterator.remove();
                break;
            }
        }
    }

    @Override
    public void clear(String idProject) {
        Iterator<ru.trifonov.tm.entity.Task> taskIterator = tasks.iterator();
        while(taskIterator.hasNext()) {
            ru.trifonov.tm.entity.Task nextTask = taskIterator.next();
            if (nextTask.getIdProject().equals(idProject)) {
                taskIterator.remove();
            }
        }
    }

    @Override
    public String generateRandomBasedUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    public void clearAll() {
        tasks.removeAll(tasks);
    }
}
