package ru.trifonov.tm.api;

public interface TaskRepository {
    void create(String name, String id, String idProject, String description, String startDate, String finishDate);
    void update(String name, String id, String idProject, String description, String startDate, String finishDate);
    void read(String idProject);
    void delete(String id);
    void clear(String idProject);
    String generateRandomBasedUUID();
    void clearAll();
}
