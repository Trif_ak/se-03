package ru.trifonov.tm.api;

public interface ProjectRepository {
    void create(String name, String id, String description, String startDate, String finishDate);
    void update(String name, String id, String description, String startDate, String finishDate);
    void read();
    void delete(String id);
    void clear();
    String generateRandomBasedUUID();
}
